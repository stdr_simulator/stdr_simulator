#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include <vector>
#include <sstream>
using namespace std;

ros::Publisher pb;
geometry_msgs::Pose2D pose;
double dt;

void dt_callback( const std_msgs::Float64::ConstPtr& msg)
{
    dt = msg->data;
}
void pose_callback( const geometry_msgs::Pose2D::ConstPtr& msg )
{
    pose.x = msg->x;
    pose.y = msg->y;
    pose.theta = msg->theta;
}

void mec_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    if( msg->layout.dim[0].size != 7 )
    {
            ROS_INFO("Number of Kinematic Parameters not equal to 7");
            return;
    }
    geometry_msgs::Twist t_msg;
    double fr, br, bl, fl, r, l1, l2;



    fl = msg->data[0];
    fr = msg->data[1];
    bl = msg->data[2];
    br = msg->data[3];
    r = msg->data[4];
    l1 = msg->data[5];
    l2 = msg->data[6];
    if( (l1 == 0 && l2 == 0) || r == 0)
            return;

    double rdt = r*dt/4.0;
    double A = fl + fr + bl + br;
    double B = -fl + fr + bl - br;
    double C = -fl + fr - bl + br;


    t_msg.linear.x  = rdt * (A * cos( pose.theta ) - B * sin( pose.theta ));
                        /* rdt*( 
                        ( cos(pose.theta) + sin(pose.theta) )*fl+
                        ( cos(pose.theta) - sin(pose.theta) )*fr+
                        ( cos(pose.theta) - sin(pose.theta) )*bl+
                        ( cos(pose.theta) + sin(pose.theta) )*br 
                        );*/
    t_msg.linear.y = rdt* (A * sin( pose.theta ) + B * cos( pose.theta ));
                    /*  rdt*( 
                        ( sin(pose.theta) - cos(pose.theta) )*fl+
                        ( sin(pose.theta) + cos(pose.theta) )*fr+
                        ( sin(pose.theta) + cos(pose.theta) )*bl+
                        ( sin(pose.theta) - cos(pose.theta) )*br 
                        );*/
    t_msg.angular.z =  rdt * C / (l1+l2 );
            //rdt/(l1+l2)* ( C );
    


    //Unused parts
    t_msg.linear.z = t_msg.angular.x = t_msg.angular.y = 0;

    pb.publish(t_msg);
}





int main(int argc, char **argv)
{
  ros::init(argc, argv, "stdr_omnifk", ros::init_options::AnonymousName);

  ros::NodeHandle nh;
  pose.theta = 0;
  pb = nh.advertise<geometry_msgs::Twist>( "robot0/cmd_vel",10);
  ros::Subscriber deltat = nh.subscribe("robot0/dt", 10, &dt_callback);
  ros::Subscriber params = nh.subscribe("robot0/kinematic_params", 10, &mec_callback);
  ros::Subscriber poses = nh.subscribe("robot0/pose2D",10, &pose_callback);
  ros::spin();


  return 0;
}
