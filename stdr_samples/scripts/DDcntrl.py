import Tkinter as Tk
import rospy
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension

def bye():
    """
    Close the application
    """
    exit()

def clear():
    """
    Resets the sliders to 0 and
    publishes a twist message with all values set to 0
    """
    global speed1, speed2, radius, length
    speed1.set(0)
    speed2.set(0)
    paramlist = [0,0,radius.get(),length.get()]
    data = Float64MultiArray(data=[])
    data.layout = MultiArrayLayout()
    data.layout.dim= [MultiArrayDimension()]
    data.layout.dim[0].label = "Parameters"
    data.layout.dim[0].size = 4
    data.layout.dim[0].stride = 1
    data.data = paramlist
    pub.publish( data )


def update(foo):
    """
    Updates the message to be published when the slider values change
    """
    global speed1, speed2, radius, length
    """Scales the wheel velocities to be between -0.5 and 0.5"""
    paramlist = [speed1.get()/200, speed2.get()/200, radius.get(), length.get()]
    data = Float64MultiArray(data=[])
    data.layout = MultiArrayLayout()
    data.layout.dim= [MultiArrayDimension()]
    data.layout.dim[0].label = "Parameters"
    data.layout.dim[0].size = 4
    data.layout.dim[0].stride = 1
    data.data = paramlist
    """Publish the twist message on robot0/cmd_vel"""
    pub.publish( data )

"""Creates the GUI window"""
root = Tk.Tk()
TITLE = Tk.Frame(root)
CMD = Tk.Frame(root)
NAV = Tk.Frame(root)
SIZE = Tk.Frame(root)
TITLE.pack(side = Tk.TOP)
CMD.pack(side = Tk.TOP)
NAV.pack(side = Tk.TOP)
SIZE.pack(side = Tk.BOTTOM)

root.wm_title("Vel")
LabelName = Tk.Label(TITLE, text="Bot Wheel Control")
LabelName.pack()

"""Creates the buttons to clear and quit the application"""
Clear = Tk.Button(CMD, text ="Clear", command = clear)
Clear.grid(row=1, column=1, sticky=Tk.E)
Q = Tk.Button(CMD, text ="Quit", command = bye)
Q.grid(row=1, column=2, sticky=Tk.E)

"""Titles for each slider are created"""
LabelServo1 = Tk.Label(NAV, text="Right")
LabelServo1.grid(row=1, column=2, sticky=Tk.E)
LabelServo2 = Tk.Label(NAV, text="Left")
LabelServo2.grid(row=1, column=1, sticky=Tk.E)


"""Creates the sliders and registers the update callback function"""
speed1 = Tk.DoubleVar()
speed2 = Tk.DoubleVar()
servo1 = Tk.Scale(NAV, from_=100, to=-100, variable = speed2, command = update)
servo1.grid(row=2, column=1, sticky=Tk.W)
servo2 = Tk.Scale(NAV, from_=100, to=-100, variable = speed1, command = update)
servo2.grid(row=2, column=2, sticky=Tk.W)


"""The entry boxes for the radius and length (on bottom of GUI)"""
radius = Tk.DoubleVar()
length = Tk.DoubleVar()
LabelInput1 = Tk.Label(SIZE, text="Radius: ")
LabelInput1.grid(row=1, column=1)
Entry1 = Tk.Entry(SIZE, textvariable=radius, width=4)
Entry1.grid(row=1, column = 2)
LabelInput2 = Tk.Label(SIZE, text="Length: ")
LabelInput2.grid(row=2, column = 1)
Entry2 = Tk.Entry(SIZE, textvariable=length, width=4)
Entry2.grid(row=2, column = 2)



"""A ROS publisher is created to send twist messages on the robot cmd_vel topic"""
pub = rospy.Publisher('robot0/kinematic_params', Float64MultiArray, queue_size=10)

"""Initializes the ROS node and gives it a unique identifier by adding random numbers to the end of the name"""
rospy.init_node('DiffDriveCntrl', anonymous=True)
layout = MultiArrayLayout()
layout.dim.insert ( 0 , [ MultiArrayDimension ( ) ] )


Tk.mainloop()
