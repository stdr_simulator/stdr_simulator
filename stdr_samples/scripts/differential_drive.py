import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64MultiArray
from math import *
pub = rospy.Publisher( 'robot0/cmd_vel', Twist, queue_size = 1)

theta = 0


def dd_callback( params ):
        global theta 
        twist_msg = Twist()
        
        w1 = params.data[0]
        w2 = params.data[1]
        wheelRadius = params.data[2]
        axelLength = params.data[3]

        twist_msg.linear.x =( wheelRadius / 2.0 * (w1 + w2)) * cos(theta)
        twist_msg.linear.y =( wheelRadius / 2.0 * (w1 + w2)) * sin(theta)
        twist_msg.angular.z =( wheelRadius/ (2.0 * axelLength))* ( w1 - w2 )
        
        """Unused parts of twist message"""
        twist_msg.linear.z = 0
        twist_msg.angular.x = 0
        twist_msg.angular.y = 0
        
        """Update prev_theta"""
        theta += twist_msg.angular.z

        pub.publish( twist_msg )


def listener():
        rospy.init_node( 'DDFK', anonymous=True )
        sub = rospy.Subscriber( 'kinematic_params', Float64MultiArray, dd_callback)
        rospy.spin()

if __name__ == '__main__':
        listener()



