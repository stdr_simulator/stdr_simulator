#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Pose2D
from sensor_msgs.msg import LaserScan




#Float64 for wheel velocities
rw_vel = 0.0
lw_vel = 0.0

#Pose2D (x, y, theta) for beacon location relative to robot location
beacon_found = False
beacon_pose = Pose2D()
beacon_pose.x = 0
beacon_pose.y = 0
beacon_pose.theta = -180

#LaserScan information on the robot laserscan taopic
laser_scan = LaserScan()



#Get the sensor reading on the left wheel velocity
def lw_callback( lw_data ):
	global lw_vel
	lw_vel = lw_data.data
	print lw_vel


#Get the sensor reading on the right wheel velocity
def rw_callback( rw_data ):
	global rw_vel
	rw_vel = lw_data.data
	print rw_vel


#Read the beacon sensor. If theta is not between -90, 90 degrees beacon is not found
def beacon_callback(beacon_data ):
	global beacon_found, beacon_pose
	#the beacon is not detected if theta is -180
	print beacon_data.theta
	if( beacon_data.theta == -180 ):
		beacon_found = False
		return
	beacon_found = True
	beacon_pose.x = beacon_data.x
	beacon_pose.y = beacon_data.y
	#The correct value will be sent in radians from [-pi/2, pi/2]
	beacon_pose.theta = beacon_data.theta




# Single scan from a planar laser range-finder
#
#
#	The following is the list of data fields associtated with the laser scan message object
#

#Header header            # timestamp in the header is the acquisition time of 
                         # the first ray in the scan.
                         
#float32 angle_min        # start angle of the scan [rad]
#float32 angle_max        # end angle of the scan [rad]
#float32 angle_increment  # angular distance between measurements [rad]

#float32 time_increment   # time between measurements [seconds] - if your scanner
                         # is moving, this will be used in interpolating position
                         # of 3d points
#float32 scan_time        # time between scans [seconds]

#float32 range_min        # minimum range value [m]
#float32 range_max        # maximum range value [m]

#float32[] ranges         # range data [m] (Note: values < range_min or > range_max should be discarded)
#float32[] intensities    # intensity data [device-specific units].  If your
                         # device does not provide intensities, please leave
                         # the array empty.


#Reads in the laser message sent on robot0/laser_0 topic
def lidar_callback( lidar_data ):
	global laser_scan
	laser_scan = lidar_data
    #print laser_scan.header
	


    
def listener():
	#create the node
    rospy.init_node('read_sensors', anonymous=True)
	#create the subscribers
    rospy.Subscriber("/robot0/left_wheel",Float64,lw_callback)
    rospy.Subscriber("/robot0/right_wheel",Float64,rw_callback)
    rospy.Subscriber("/robot0/beacon",Pose2D, beacon_callback)
    rospy.Subscriber("/robot0/laser_0",LaserScan, lidar_callback) 

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

	#use sleep instead and use a while ros ok loop if you want to grab everything then do a computation 
	#and publish a message based on all subscribers

if __name__ == '__main__':
    listener()
