import Tkinter as Tk
import rospy
from geometry_msgs.msg import Twist


def bye():
    """
    Close the application
    """
    exit()

def clear():
    """
    Resets the sliders to 0 and 
    publishes a twist message with all values set to 0
    """
    speed1.set(0)
    speed2.set(0)
    speed3.set(0)
    twist_msg = Twist()
    twist_msg.linear.x = 0
    twist_msg.linear.y = 0
    twist_msg.linear.z = 0
    twist_msg.angular.x = 0
    twist_msg.angular.y = 0
    twist_msg.angular.z = 0
    pub.publish( twist_msg )   


def update(foo):
    """
    Updates the twist message to be published when the slider values change
    """
    twist_msg = Twist()
    """Scales the x velocity to be between -0.5 and 0.5"""
    twist_msg.linear.x = speed1.get()/200
    twist_msg.linear.y = speed2.get()/200
    
    """Unused twist fields"""
    twist_msg.linear.z = 0
    twist_msg.angular.x = 0
    twist_msg.angular.y = 0
    
    """Scales the angular z (Theta) velocity to be between -0.5 and 0.5"""
    twist_msg.angular.z = speed3.get()/200
    
    """Publish the twist message on robot0/cmd_vel"""
    pub.publish( twist_msg )

"""Creates the GUI window"""
root = Tk.Tk()
TITLE = Tk.Frame(root)
CMD = Tk.Frame(root)
NAV = Tk.Frame(root)
TITLE.pack(side = Tk.TOP)
CMD.pack(side = Tk.TOP)
NAV.pack(side = Tk.BOTTOM)
root.wm_title("Vel")
LabelName = Tk.Label(TITLE, text="Bot speed Control")
LabelName.pack()

"""Creates the buttons to clear and quit the application"""
Clear = Tk.Button(CMD, text ="Clear", command = clear)
Clear.grid(row=1, column=1, sticky=Tk.E)
Q = Tk.Button(CMD, text ="Quit", command = bye)
Q.grid(row=1, column=2, sticky=Tk.E)

"""Creates the sliders and registers the update callback function"""
speed1 = Tk.DoubleVar()
speed2 = Tk.DoubleVar()
speed3 = Tk.DoubleVar()
servo1 = Tk.Scale(NAV, from_=100, to=-100, variable = speed1, command = update)
servo1.grid(row=1, column=1, sticky=Tk.W)
servo2 = Tk.Scale(NAV, from_=100, to=-100, variable = speed2, command = update)
servo2.grid(row=1, column=2, sticky=Tk.W)
servo3 = Tk.Scale(NAV, from_=100, to=-100, variable = speed3, command = update)
servo3.grid(row=1, column=3, sticky=Tk.W)
"""Titles for each slider are created"""
LabelServo1 = Tk.Label(CMD, text="Vel X")
LabelServo1.grid(row=2, column=1, sticky=Tk.E)
LabelServo2 = Tk.Label(CMD, text="Vel Y")
LabelServo2.grid(row=2, column=2, sticky=Tk.E)
LabelServo3 = Tk.Label(CMD, text="Theta")
LabelServo3.grid(row=2, column=3, sticky=Tk.E)

"""A ROS publisher is created to send twist messages on the robot cmd_vel topic"""
pub = rospy.Publisher('robot0/cmd_vel', Twist, queue_size=10)

"""Initializes the ROS node and gives it a unique identifier by adding random numbers to the end of the name"""
rospy.init_node('SpeedCntrl', anonymous=True)

Tk.mainloop()


