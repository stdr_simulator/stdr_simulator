import rospy
from math import *
import numpy as np
from std_msgs.msg import Float64MultiArray
from std_msgs.msg import MultiArrayLayout
from std_msgs.msg import MultiArrayDimension
r = 2.0
l = 3.0

def talker(w1, w2, r, l):

    pub = rospy.Publisher('kinematic_params', Float64MultiArray, queue_size=1)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    layout = MultiArrayLayout()
    layout.dim.insert(0, [MultiArrayDimension()] )
    while not rospy.is_shutdown():
        data = Float64MultiArray(data=[])
        data.layout = MultiArrayLayout()
        data.layout.dim = [MultiArrayDimension()]
        data.layout.dim[0].label = "Parameters"
        data.layout.dim[0].size = 4
        data.layout.dim[0].stride = 1
        data.data = [w1,w2,r,l]
        pub.publish(data)
        rate.sleep()

if __name__ == '__main__':
        try:
                talker(1.5,1.0,r,l)
        except rospy.ROSInterruptException:
                pass
