#include "ros/ros.h"
#include "geometry_msgs/Pose2D.h"
#include "geometry_msgs/Point.h"
#include "nav_msgs/MapMetaData.h"
#include <float.h>
#include <vector>
#include <sstream>
#include <stdlib.h>     /* srand, rand */
#include <time.h> 
using namespace std;

ros::Publisher pb;
geometry_msgs::Pose2D pose;
geometry_msgs::Pose2D beacon;
geometry_msgs::Pose2D loc_pose;
int location = 0;
double res = 0;

void resolution(nav_msgs::MapMetaData msg)
{
    res = msg.resolution;
}

double dist( geometry_msgs::Pose2D p1, geometry_msgs::Pose2D p2)
{
    return (p1.x - p2.x)*(p1.x-p2.x) + (p1.y - p2.y) * (p1.y-p2.y);
}

void zero_to_2PI( double &theta )
{
        while(theta < 0 || theta > 2*M_PI)
        {
                if( theta < 0 )
                        theta+= 2*M_PI;
                else
                        theta-=2*M_PI;
        }
}
double angle_btw( geometry_msgs::Pose2D p1, geometry_msgs::Pose2D p2)
{
        double tmp = atan2(p2.y - p1.y, p2.x - p1.x) - p1.theta+ M_PI/2;
        zero_to_2PI(tmp);
        return tmp;
}

void pose_callback( const geometry_msgs::Pose2D::ConstPtr& msg )
{
    double tx, ty, tt;
    pose.x = msg->x;
    pose.y = msg->y;
    pose.theta = msg->theta;
    zero_to_2PI(pose.theta);
    loc_pose.x = loc_pose.y = 0;
    loc_pose.theta = -180;
    if( dist( beacon, pose)*1 > 4 )
    {
            pb.publish(loc_pose);
            return;
    }
    tt = atan2(pose.y - beacon.y, pose.x-beacon.x) - pose.theta;
    tx = (beacon.x - pose.x) * cos(pose.theta) + (beacon.y - pose.y) * sin(pose.theta);
    ty = -(beacon.x - pose.x) * sin(pose.theta) + (beacon.y - pose.y) * cos(pose.theta);
    
    if( tx < 0 )
    {
        pb.publish(loc_pose);
        return;
    }
    loc_pose.x = tx;
    loc_pose.y = ty;
    loc_pose.theta = tt;

    pb.publish( loc_pose );
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "stdr_beacon", ros::init_options::AnonymousName);
  
  ros::NodeHandle nh;
  
  pb = nh.advertise<geometry_msgs::Pose2D>( "robot0/beacon",10);
  ros::Subscriber poses = nh.subscribe("robot0/pose2D",10, &pose_callback);
  ros::Subscriber resol = nh.subscribe("map_metadata", 10, &resolution);
  srand(time(NULL));
  double x[] = {51.6630632896, 16.6000003815, 57.2000007629};
  double y[] = {51.0571013988, 110.4000001526, 80.2000045776};
  double th[] = {-180, -180, -180};
  switch(argv[1][0])
  {
          case '1': location = 0;
                    break;
          case '2': location = 1;
                    break;
          case '3': location = 2;
                    break;
          default:  location = rand()%3;
  };
  beacon.x = x[location];
  beacon.y = y[location];
  beacon.theta = th[location]; 


  loc_pose.x = loc_pose.y = 0;
  loc_pose.theta = -180;

  ros::spin();


  return 0;
}
