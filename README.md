stdr_simulator [![Build Status](https://travis-ci.org/stdr-simulator-ros-pkg/stdr_simulator.png?branch=indigo-devel)](https://travis-ci.org/stdr-simulator-ros-pkg/stdr_simulator)
==============

A simple, flexible and scalable 2D multi-robot simulator.

## Documentation and Tutorials
[ROS wiki page](http://wiki.ros.org/stdr_simulator)

## Further reference
[External website](http://stdr-simulator-ros-pkg.github.io/)

[Google Group](https://groups.google.com/forum/#!forum/stdr-simulator)

After cloning this repository make sure to run the following commands:
~~~~
cd <your_catkin_ws>
cd ..
rosdep install --from-paths src --ignore-src --rosdistro $ROS_DISTRO
catkin_make
~~~~


