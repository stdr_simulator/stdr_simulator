#include "ros/ros.h"
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "std_msgs/Float64.h"
#include <random>

#include <vector>
#include <sstream>
using namespace std;

ros::Publisher pb;
ros::Publisher lw_pb;
ros::Publisher rw_pb;
geometry_msgs::Pose2D pose;
double dt;

default_random_engine generator;
normal_distribution<double> distribution(0.0, 10.0/100);
normal_distribution<double> sensor_dist( 0.0, 5.0/100);
void dt_callback( const std_msgs::Float64::ConstPtr& msg)
{
    dt = msg->data;
}

void pose_callback( const geometry_msgs::Pose2D::ConstPtr& msg )
{
    pose.x = msg->x;
    pose.y = msg->y;
    pose.theta = msg->theta;
}

void dd_callback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    if( msg->layout.dim[0].size != 4 )
    {
            ROS_INFO("Number of Kinematic Parameters not equal to 4");
            return;
    }
    geometry_msgs::Twist t_msg;
    double w1, w2, r=.0000001, l=0.0000001;
    double tmp1 = 0, tmp2 = 0;
    w1 = msg->data[0];
    w2 = msg->data[1];
    r = msg->data[2];
    l = msg->data[3];
    if(l == 0)
        return;

    //Normal distribution of noise on left and right wheel velocities.
    double number = distribution(generator);
    if(w1 != 0 && w2 != 0)
    {
        w1 += (distribution(generator));
        w2 += (distribution(generator));
    }

    t_msg.linear.x  = r*dt*(w1 + w2)*cos(pose.theta)/2;
    t_msg.linear.y  = r*dt*(w1 + w2)*sin(pose.theta)/2;
    t_msg.angular.z = r*dt*(w1 - w2)/(2*l);
    
    //Unused parts
    t_msg.linear.z = t_msg.angular.x = t_msg.angular.y = 0;
    std_msgs::Float64 t1, t2;
    if( w1 != 0 && w2 != 0)
    {
        tmp1 = sensor_dist(generator);
        tmp2 = sensor_dist(generator);
    }
    t1.data = w1 + tmp1; 
    t2.data = w2 + tmp2;
    rw_pb.publish(t1);
    lw_pb.publish(t2); 
    pb.publish(t_msg);
}





int main(int argc, char **argv)
{
  ros::init(argc, argv, "stdr_ddfk", ros::init_options::AnonymousName);

  ros::NodeHandle nh;
  pose.theta = 0;
  pb = nh.advertise<geometry_msgs::Twist>( "robot0/cmd_vel",10);
  rw_pb = nh.advertise<std_msgs::Float64>( "robot0/right_wheel", 10);
  lw_pb = nh.advertise<std_msgs::Float64>( "robot0/left_wheel", 10);
  ros::Subscriber deltat = nh.subscribe("robot0/dt",10, &dt_callback);

  ros::Subscriber params = nh.subscribe("robot0/kinematic_params", 10, &dd_callback);
  
  ros::Subscriber poses = nh.subscribe("robot0/pose2D",10, &pose_callback);
  ros::spin();


  return 0;
}
